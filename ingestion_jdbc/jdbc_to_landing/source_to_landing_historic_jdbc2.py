import traceback
from datetime import datetime

from pyspark.sql import SparkSession
from pyspark.sql.functions import *
import s3fs
import numpy as np

from ingestion_jdbc.ingestion_jdbc import IngestionJDBC
from utils import utils
from utils import errors


class JDBCToLandingHistoric2(IngestionJDBC):
    def __init__(self, env: str, logger, logger_io_buffer, spark: SparkSession):
        IngestionJDBC.__init__(self, env, logger, logger_io_buffer)  # Using constructor Base Classe
        self.spark = spark
        self.layer_destination = 'landing'
        self.delimiter = '\001'
        self.path_landing = None

    def run_ingestion(self,
                      system_table: str,
                      partition: str,
                      reproc: bool = False):
        try:
            # Create variables
            self.system_table = system_table
            self.system_table = self.system_table.lower()
            schema = self.system_table.rsplit(".", 1)[0]
            table_source = self.system_table.rsplit(".", 1)[1]
            if schema in table_source:
                table_source = table_source.replace(schema + '_', "")

            # Get data from params file and check
            cad_tabelas_params = self.load_s3_cad_tabelas_params()
            delta_column1 = cad_tabelas_params['delta_column']
            delta_column2 = cad_tabelas_params['delta_column2']
            delta_operation = cad_tabelas_params['delta_operation']
            database = cad_tabelas_params['servidor'].upper()

            params = self.load_s3_jdbc_credentials_params(database)
            username = params['username']
            password = params['password']
            jdbc_url = params['jdbc_url']
            driver = params['v_driver']
            provider = params['provider'].lower()
            if not provider:
                raise utils.errors.ParamsFile("Provider not implemented in credentials file.")

            # Specify the load type (Delta or Full)
            # self.load_type is FULL by default. Case where is filled in cad_tabelas.csv then,
            # self.load_type change to DELTA
            self.load_type = "FULL"
            self.load_type = "DELTA" if delta_column1 else self.load_type

            # Create partition list for inserting
            partition_list = self._get_partition_to_insert(partition)

            if self.load_type == 'FULL' and len(partition_list) > 1:
                msg_error = f"The {self.system_table} is FULL. " \
                            f"Use the engine_ingestion.py."
                raise utils.errors.InvalidLakeAction(msg_error)

            if not delta_column1:
                msg_error = "Register the delta column in cad_tabelas.csv"
                raise utils.errors.ParamsFile(msg_error)

            query = f"SELECT * FROM {schema}.{table_source}"
            """
            provider="oracle"
            partition_list = ['20200101', '20200110']
            delta_column1 = "col1"
            delta_column2 = "col2"
            delta_column2 = None
            """

            if provider == 'oracle':
                new_where_c1 = f"trunc({delta_column1})>=TO_DATE('{partition_list[0]}', 'YYYYmmdd')"
                new_where = f"{new_where_c1} AND trunc({delta_column1})<=TO_DATE('{partition_list[-1]}', 'YYYYmmdd')"
                if delta_column2:
                    new_where_c1_list = new_where.split("AND")
                    cond1 = new_where_c1_list[0].strip()
                    cond2 = f"trunc({delta_column2})>=TO_DATE('{partition_list[0]}', 'YYYYmmdd')"
                    cond3 = new_where_c1_list[1].strip()
                    cond4 = f"trunc({delta_column2})<=TO_DATE('{partition_list[-1]}', 'YYYYmmdd')"
                    new_where = f"({cond1} {delta_operation} {cond2}) AND ({cond3} {delta_operation} {cond4})"
            elif provider == 'sqlserver':
                new_where_c1 = f"CONVERT(DATETIME, CONVERT(DATE, {delta_column1}))>=CONVERT(DATETIME, '{partition_list[0]}')"
                new_where = f"{new_where_c1} AND CONVERT(DATETIME, CONVERT(DATE, {delta_column1}))<=CONVERT(DATETIME, '{partition_list[-1]}')"
                if delta_column2:
                    new_where_c1_list = new_where.split("AND")
                    cond1 = new_where_c1_list[0].strip()
                    cond2 = f"CONVERT(DATETIME, CONVERT(DATE, {delta_column2}))>=CONVERT(DATETIME, '{partition_list[0]}')"
                    cond3 = new_where_c1_list[1].strip()
                    cond4 = f"CONVERT(DATETIME, CONVERT(DATE, {delta_column2}))>=CONVERT(DATETIME, '{partition_list[-1]}')"
                    new_where = f"({cond1} {delta_operation} {cond2}) AND ({cond3} {delta_operation} {cond4})"
            elif provider == 'postgresql':
                new_where_c1 = f"DATE_TRUNC('day', {delta_column1})>=TO_DATE('{partition_list[0]}', 'YYYYmmdd')"
                new_where = f"{new_where_c1} AND DATE_TRUNC('day', {delta_column1})<=TO_DATE('{partition_list[-1]}', 'YYYYmmdd')"
                if delta_column2:
                    new_where_c1_list = new_where.split("AND")
                    cond1 = new_where_c1_list[0].strip()
                    cond2 = f"DATE_TRUNC('day', {delta_column2})>=TO_DATE('{partition_list[0]}', 'YYYYmmdd')"
                    cond3 = new_where_c1_list[1].strip()
                    cond4 = f"DATE_TRUNC('day', {delta_column2})>=TO_DATE('{partition_list[-1]}', 'YYYYmmdd')"
                    new_where = f"({cond1} {delta_operation} {cond2}) AND ({cond3} {delta_operation} {cond4})"
            elif provider == 'sap':
                new_where_c1 = f"TO_VARCHAR({delta_column1}, 'YYYYMMDD')>=TO_VARCHAR('{partition_list[0]}', 'YYYYMMDD')"
                new_where = f"{new_where_c1} AND TO_VARCHAR({delta_column1}, 'YYYYMMDD')<=TO_VARCHAR('{partition_list[-1]}', 'YYYYMMDD')"
                if delta_column2:
                    new_where_c1_list = new_where.split("AND")
                    cond1 = new_where_c1_list[0].strip()
                    cond2 = f"TO_VARCHAR({delta_column2}, 'YYYYMMDD')>=TO_VARCHAR('{partition_list[0]}', 'YYYYMMDD')"
                    cond3 = new_where_c1_list[1].strip()
                    cond4 = f"TO_VARCHAR({delta_column2}, 'YYYYMMDD')>=TO_VARCHAR('{partition_list[-1]}', 'YYYYMMDD')"
                    new_where = f"({cond1} {delta_operation} {cond2}) AND ({cond3} {delta_operation} {cond4})"
            else:
                raise utils.errors.InvalidLakeAction("Provider not implemented in engine ingestion.")
            """
            elif provider == 'mysql':
                new_where = f"DATE({column})=STR_TO_DATE('{ref}', '%Y%mm%dd')"
            """
            query = f"{query} where {new_where}"
            print(f"\n\nQuery = {query}")
            df = self.spark.read.format("jdbc") \
                .option("user", username) \
                .option("password", password) \
                .option("url", jdbc_url) \
                .option("driver", driver) \
                .option("fetchsize", 100000) \
                .option("query", query).load().cache()

            count = df.count()
            msg = f"Partition = {partition}. Rows in memory from JDBC= {count}"
            self.logger.info(msg)
            self.logger_io_buffer.write(msg)

            for c in df.columns:
                df = df.withColumn(c, regexp_replace(c, '\r\n', " "))
                df = df.withColumn(c, regexp_replace(c, '\n', " "))
            df.write.parquet(path=f"s3://natura-datalake-{self.env}/try/historic_temp/h_{self.system_table}/", mode="overwrite")
            del df
            ##############################################
            df = self.spark.read.load(f"s3://natura-datalake-prd/try/historic_temp/h_{self.system_table}/").cache()
            df = df.withColumn("anomesdia", date_format(df[delta_column1], "yyyyMMdd"))
            msg = f"Partition = {partition}. Rows in memory from temp file= {df.count()}"
            self.logger.info(msg)
            self.logger_io_buffer.write(msg)

            total_registers = 0
            for ref in partition_list:
                dh = datetime.today().strftime("%d/%m/%Y - %H:%M:%S")
                df_ref = df.filter(df['anomesdia'] == ref)
                df_ref = df_ref.drop('anomesdia')
                self.path_landing = f"""s3://{self.bucket_name}/landing/{self.system_table}-{ref}/"""
                # count = df_ref.count()
                # msg = f"{ref} - {count}\n"
                # self.logger.info(msg)
                # self.logger_io_buffer.write(msg)
                df_ref.write \
                    .mode("overwrite") \
                    .format("csv") \
                    .option("delimiter", self.delimiter) \
                    .option("nullValue", None) \
                    .option("emptyValue", None) \
                    .option("timestampFormat", "yyyy-MM-dd HH:mm:ss") \
                    .save(self.path_landing)
                self.logger.info(f"{dh} --> {ref}")
                del df_ref
                #total_registers += count
            # msg = f"TOTAL ROWS = {total_registers}"
            # self.logger.info(msg)
            # self.logger_io_buffer.write(msg)
            s3 = s3fs.S3FileSystem()
            s3.rm(path=f"s3://natura-datalake-{self.env}/try/historic_temp/h_{self.system_table}/", recursive=True)
            return 0
        except Exception as msg_error:
            traceback.print_exc()
            return -1
