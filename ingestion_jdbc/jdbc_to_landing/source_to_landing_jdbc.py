import traceback
import math
import time
import s3fs
import numpy as np
import py4j
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from ingestion_jdbc.ingestion_jdbc import IngestionJDBC
from utils import utils
from utils import errors


class JDBCToLanding(IngestionJDBC):
    def __init__(self, env: str, logger, logger_io_buffer, spark: SparkSession):
        IngestionJDBC.__init__(self, env, logger, logger_io_buffer)  # Using constructor Base Classe
        self.spark = spark
        self.layer_destination = 'landing'
        self.delimiter = '\001'
        self.path_landing = None

    def _check_count_source_landing_jdbc(self, database, ref):
        table = self.system_table.rsplit(".", 1)[1]
        self.count_destination = self.spark.read.csv(self.path_landing, header=False, sep=self.delimiter).count()
        diff_rows = self.count_source - self.count_destination
        diff_rows = int(math.fabs(diff_rows))
        if self.count_source == self.count_destination:
            msg_log = f"""
\n###########################
SUCCESSFULLY: NUMBER OF ROWS IN {database}.{self.system_table} AND {self.layer_destination} ARE MATCH.
Table {self.layer_destination}.{table} saved in {self.path_landing}!
Partition => anomesdia = {ref}
Number of rows saved = {self.count_destination}"""
            self.logger.info(msg_log)
            self.logger_io_buffer.write(msg_log)
#         elif diff_rows <= 10000:
#             msg_log = f"""
# \n###########################
# SUCCESSFULLY: NUMBER OF ROWS IN {database}.{self.system_table} AND {self.layer_destination}
# ARE MATCH WITH SOME DELTA ROWS.
# Table {self.layer_destination}.{table} saved in {self.path_landing}!
# Partition => anomesdia = {ref}
# Number of rows in {database}.{self.system_table} = {self.count_source}
# Number of rows saved = {self.count_destination}
# Number of different rows = {diff_rows}"""
#             self.logger.info(msg_log)
#             self.logger_io_buffer.write(msg_log)
        else:
            # Remove from landing
            s3 = s3fs.S3FileSystem()
            s3.rm(path=self.path_landing, recursive=True)
            error_msg = f"Number of rows in {database}.{self.system_table} and {self.layer_destination} aren't match."
            msg_log = f"""
\n###########################
ERROR: {error_msg}
Number of rows in {database}.{self.system_table} = {self.count_source}
Number of rows in {self.layer_destination} = {self.count_destination}
Partition {ref} removed
###########################"""
            self.logger.info(msg_log)
            self.logger_io_buffer.write(msg_log)
            raise errors.CountMatch(error_msg)

    def _transform(self, df):
        for c in df.columns:
            df = df.withColumn(c, regexp_replace(c, '\r\n', " "))
            df = df.withColumn(c, regexp_replace(c, '\n', " "))
            #df = df.withColumn(c, regexp_replace(c, '\r', " "))
            # df = df.withColumn(c, regexp_replace(c, '\\\n', " "))
        return df

    def _create_where_condition(self, provider, delta_column, ref):
        # where trunc(TS_ULTIMA_ATUALIZACAO)=TO_DATE('2020-08-05', 'YYYY-mm-dd')
        new_where = ""
        if provider == 'oracle':
            new_where = f"trunc({delta_column})=TO_DATE('{ref}', 'YYYYmmdd')"
        elif provider == 'sqlserver':
            new_where = f"CONVERT(DATETIME, CONVERT(DATE, {delta_column}))=CONVERT(DATETIME, '{ref}')"
        elif provider == 'postgresql':
            new_where = f"DATE_TRUNC('day', {delta_column})=TO_DATE('{ref}', 'YYYYmmdd')"
        elif provider == 'sap':
            new_where = f"TO_VARCHAR({delta_column},'YYYYMMDD')=TO_VARCHAR('{ref}', 'YYYYMMDD')"
        elif provider == 'mysql':
            new_where = f"DATE({delta_column})=STR_TO_DATE('{ref}', '%Y%m%d')"
        else:
            raise utils.errors.InvalidLakeAction("Provider not implemented in engine ingestion.")
        
        return new_where

    def _create_where_condition_two_columns(self, provider, delta_column1, delta_column2, delta_operation, ref):
        # where trunc(TS_ULTIMA_ATUALIZACAO)=TO_DATE('2020-08-05', 'YYYY-mm-dd')
        new_where = ""
        if provider == 'oracle':
            new_where = f"trunc({delta_column1})=TO_DATE('{ref}', 'YYYYmmdd')"
            if delta_column2:
                new_where += f" {delta_operation} trunc({delta_column2})=TO_DATE('{ref}', 'YYYYmmdd')"
        elif provider == 'sqlserver':
            new_where = f"CONVERT(DATETIME, CONVERT(DATE, {delta_column1}))=CONVERT(DATETIME, '{ref}')"
            if delta_column2:
                new_where += f" {delta_operation} CONVERT(DATETIME, CONVERT(DATE, {delta_column2}))=CONVERT(DATETIME, '{ref}')"
        elif provider == 'postgresql':
            new_where = f"DATE_TRUNC('day', {delta_column1})=TO_DATE('{ref}', 'YYYYmmdd')"
            if delta_column2:
                new_where += f" {delta_operation} DATE_TRUNC('day', {delta_column2})=TO_DATE('{ref}', 'YYYYmmdd')"
        elif provider == 'sap':
            new_where = f"TO_VARCHAR({delta_column1}, 'YYYYMMDD')=TO_VARCHAR('{ref}', 'YYYYMMDD')"
            if delta_column2:
                new_where += f" {delta_operation} TO_VARCHAR({delta_column2}, 'YYYYMMDD')=TO_VARCHAR('{ref}', 'YYYYMMDD')"
        elif provider == 'mysql':
            new_where = f"DATE({delta_column1})=STR_TO_DATE('{ref}', '%Y%m%d')"
            if delta_column2:
                new_where = f"{delta_operation} DATE({delta_column2})=STR_TO_DATE('{ref}', '%Y%m%d')"
        else:
            raise utils.errors.InvalidLakeAction("Provider not implemented in engine ingestion.")
                
        return new_where

    @utils.monitor_time
    def run_ingestion(self,
                      system_table: str,
                      partition: str,
                      reproc: bool = False,
                      no_tdk: bool = False):
        try:
            # Create variables
            self.system_table = system_table
            self.system_table = self.system_table.lower()
            schema = self.system_table.rsplit(".", 1)[0]
            table_source = self.system_table.rsplit(".", 1)[1]

            # Get data from params file and check
            cad_tabelas_params = self.load_s3_cad_tabelas_params()
            custom_get = cad_tabelas_params['custom_get'] # true/false
            delta_column1 = cad_tabelas_params['delta_column']
            delta_column2 = cad_tabelas_params['delta_column2']
            delta_operation = cad_tabelas_params['delta_operation']
            database = cad_tabelas_params['servidor'].upper()

            # TODO: Substituir por AWS secrets manager or parameters store
            params = self.load_s3_jdbc_credentials_params(database)
            username = params['username']
            password = params['password']
            jdbc_url = params['jdbc_url']
            driver = params['v_driver']
            provider = params['provider'].lower()

            if provider == 'mysql':
                table_source = table_source.upper()

            if schema in table_source:
                table_source = table_source.replace(schema+'_', "")
            
            if custom_get == False: # padrão False
                query = f"SELECT * FROM {schema}.{table_source}"
                query_count = f"SELECT count(*) as count FROM {schema}.{table_source}"
            if custom_get == True:
                print("CUSTOM GET MODE -> SELECTING CUSTOM COLUMNS")
                print('#'*30)
                cad_colunas_params = self.load_s3_cad_colunas_params()
                custom_columns = cad_colunas_params["custom_columns"]
                query = f"SELECT {custom_columns} FROM {schema}.{table_source}"
                query_count = f"SELECT count(*) as count FROM {schema}.{table_source}"

            if table_source == 'tmp_relvendasold':
                table_source = 'tmp_relvendasOLD'
                query = f'SELECT * FROM {schema}."{table_source}"'
                query_count = f'SELECT count(*) as count FROM {schema}."{table_source}"'

            if not provider:
                raise utils.errors.ParamsFile("Provider not implemented in credentials file.")

            # Specify the load type (Delta or Full)
            # self.load_type is FULL by default. Case where is filled in cad_tabelas.csv then,
            # self.load_type change to DELTA
            self.load_type = "FULL"
            self.load_type = "DELTA" if delta_column1 else self.load_type

            # Create partition list for inserting
            partition_list = [0]
            if partition:
                partition_list = self._get_partition_to_insert(partition)

            if self.load_type == 'FULL' and len(partition_list) > 1:
                msg_error = f"The {self.system_table} is FULL. " \
                            f"It can not be loaded more than one partition. Ajust tag -partition."
                raise utils.errors.InvalidLakeAction(msg_error)

            # Insertion!
            # TODO: Vale a pena alterar para inserção de várias partições ao mesmo tempo? Como fazer o check?
            #  df.write.partitionBy?
            total_registers = 0
            for ref in partition_list:
                self.path_landing = f"""s3://{self.bucket_name}/landing/{self.system_table}-{ref}/"""
                new_query = ""
                new_query = query
                new_query_count = query_count
                if delta_column1:
                    new_where = self._create_where_condition_two_columns(provider, delta_column1, delta_column2, delta_operation, ref)

                    new_query = f"{query} where {new_where}"
                    new_query_count = f"{query_count} where {new_where}"

                self.logger.info(f"\n\nQuery = {new_query}")
                self.logger_io_buffer.write(f"\nQuery = {new_query}")

                # Get data
                try:
                    df = self.spark.read.format("jdbc") \
                        .option("user", username) \
                        .option("password", password) \
                        .option("url", jdbc_url) \
                        .option("driver", driver) \
                        .option("fetchsize", 50000) \
                        .option("query", new_query).load().cache()
                except py4j.protocol.Py4JJavaError:
                    print("\n\n\n\n$$$$$$$$$$$$$$$$$$$$ except \n\n\n\n")
                    df = self.spark.read.format("jdbc") \
                        .option("user", username) \
                        .option("password", password) \
                        .option("url", jdbc_url) \
                        .option("driver", driver) \
                        .option("query", new_query).load().cache()

                v_count = df.count()
                self.logger.info(f"Rows in memory = {v_count}")
                self.logger_io_buffer.write(f"Rows in memory = {v_count}")
                #print(df.printSchema())

                # Count source (JDBC)
                # TODO: A quantidade de registros pode mudar entre o tempo de contagem e leitura dos dados.
                #  Se der muito problema, esse check precisa ser revisto ou até mesmo removido
                count = self.spark.read.format("jdbc") \
                    .option("user", username) \
                    .option("password", password) \
                    .option("url", jdbc_url) \
                    .option("driver", driver) \
                    .option("query", new_query_count).load()

                for col in count.columns:
                    count = count.withColumnRenamed(col, col.lower())

                self.count_source = int(count.first()['count'])
                self.logger.info(f"count_source = {self.count_source}")

                if v_count != count:
                    utils.errors.CountMatch("Number of rows in source are changing."
                                            "Try again later or change airflow schedule time.")

                # Corrige problema de quebra de linhas dentro de qualquer coluna
                df = self._transform(df)
                self.logger.info("Data in memory with break line fixed.. \nStarting write")
                # .option("timestampFormat", "yyyy-MM-dd HH:mm:ss")
                # .option("dateFormat", "yyyy-MM-dd")
                df.show(1)
                for c in df.columns:
                    df = df.withColumn(c, regexp_replace(c, '\r', " "))

                df.write \
                    .mode("overwrite") \
                    .format("csv") \
                    .option("delimiter", self.delimiter) \
                    .option("nullValue", None) \
                    .option("emptyValue", None) \
                    .option("timestampFormat", "yyyy-MM-dd HH:mm:ss") \
                    .save(self.path_landing)

                if no_tdk:
                    total_registers = v_count
                else:
                    time.sleep(15)
                    self._check_count_source_landing_jdbc(database=database, ref=ref)
                    total_registers += self.count_destination

            return 0, None, total_registers, self.load_type
        except Exception as msg_error:
            traceback.print_exc()
            self.logger.exception(msg_error)
            self.logger_io_buffer.write(traceback.format_exc())
            return -1, msg_error, 0, self.load_type
