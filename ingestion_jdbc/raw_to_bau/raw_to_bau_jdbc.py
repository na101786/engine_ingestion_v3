import traceback
import time

from pyspark.sql import SparkSession
import boto3

from ingestion_jdbc.ingestion_jdbc import IngestionJDBC
from utils import utils
from utils import errors
from logs import log_stats


class RawToBauJDBC(IngestionJDBC):
    def __init__(self, env: str, logger, logger_io_buffer, spark: SparkSession):
        IngestionJDBC.__init__(self, env, logger, logger_io_buffer)  # Using constructor Base Classe
        self.spark = spark
        self.layer_destination = 'bau'
        self.layer_source = 'raw'

    def _run_ingestion_without_partition(self, table):
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(self.bucket_name)
        file_delete = f"{self.layer_destination}/{table}/"
        bucket.objects.filter(Prefix=file_delete).delete()

        query = f"""SELECT * FROM {self.layer_source}.{table}"""
        df = self.spark.sql(query)
        df.write.mode("OVERWRITE").insertInto(f"{self.layer_destination}.{table}")
        time.sleep(10)
        total_registers = self.spark.sql(f"select * from {self.layer_destination}.{table}").count()
        msg = f"{self.system_table} saved. Rows = {total_registers}"
        self.logger.info(msg)
        self.logger_io_buffer.write(msg)
        return total_registers

    @utils.monitor_time
    def run_ingestion(self,
                      system_table: str,
                      partition: str,
                      reproc: bool = False):
        try:
            # Create variables
            self.system_table = system_table
            self.system_table = self.system_table.lower()
            table = self.system_table.rsplit(".", 1)[1]

            # Get data from params file and check
            # database, incremtal_overwrite = self._get_data_from_params_file()
            cad_tabelas_params = self.load_s3_cad_tabelas_params()
            cad_arq_params = self.load_s3_cad_arq_params()
            incremtal_overwrite = cad_arq_params['incremtal_overwrite']
            delta_column = cad_tabelas_params['delta_column']

            # Specify the load type (Delta or Full)
            # self.load_type is FULL by default. Case where is filled in cad_tabelas.csv then,
            # self.load_type change to DELTA
            self.load_type = "DELTA" if delta_column else self.load_type

            # Specify save mode
            save_mode = 'INTO'
            if incremtal_overwrite == 'O' or reproc:
                save_mode = 'OVERWRITE'

            if not partition:
                total_registers = self._run_ingestion_without_partition(table)
                return 0, None, total_registers, self.load_type

            # Create partition list for inserting
            partition_list = self._get_partition_to_insert(partition)

            # Insertion!
            # Reference for parallel process: # https://stackoverflow.com/questions/45543396/dropping-multiple-partitions-in-impala-hive

            total_registers = 0
            for ref in partition_list:
                if save_mode == 'OVERWRITE':
                    # Removing hive reference
                    self._removing_hive_partition(table, ref)

                    # Removing s3 folder
                    self._removing_s3_partition(table, ref)
                    time.sleep(10)

                # Count source (raw)
                count_raw = f"SELECT count(*) AS COUNT FROM {self.layer_source}.{table} WHERE anomesdia={ref}"
                self.count_source = self.spark.sql(count_raw).first()['COUNT']

                if self.count_source != 0:
                    # TODO: Testar se OVERWRITE appenda as informações. Na versão de shell + python nosso processo parace
                    #  que apenda,mesmo estando overwrite, por quê?
                    query = f"""SELECT * FROM {self.layer_source}.{table} WHERE anomesdia = {ref}"""
                    # insert = F"""INSERT {save_mode} TABLE {self.layer_destination}.{table} PARTITION(anomesdia) {query}"""
                    # self.logger.info(f"Executing query: {insert}")
                    # self.logger_io_buffer.write(f"\nExecuting query: {insert}")
                    # self.spark.sql(insert)
                    df = self.spark.sql(query)
                    df.write.mode(save_mode).insertInto(f"{self.layer_destination}.{table}")
                    time.sleep(10)

                    self._check_count_jdbc(system_table=self.system_table, partition=ref)
                else:
                    msg = f"SUCCESSFULLY: But the {self.layer_source} has no data to be inserted"
                    self.logger.info(msg)
                    self.logger_io_buffer.write(msg)

                total_registers += self.count_source

            return 0, None, total_registers, self.load_type
        except Exception as msg_error:
            traceback.print_exc()
            self.logger.exception(msg_error)
            return -1, msg_error, 0, self.load_type

