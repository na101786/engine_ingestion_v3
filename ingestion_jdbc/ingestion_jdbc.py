import pandas as pd
import numpy as np
import s3fs
from abc import ABC, abstractmethod
from base_class.ingestion_lake import IngestionLake
from utils import utils
from utils import errors

class IngestionJDBC(IngestionLake, ABC):
    def __init__(self, env: str, logger, logger_io_buffer):
        IngestionLake.__init__(self, env, logger, logger_io_buffer)
        self.system_table = None  # Waiting for system.table format
        self.load_type = None  # Delta or Full

    def load_s3_cad_tabelas_params(self):
        # load
        df_cad_tabelas = utils.get_repoingestion_table_as_df(
            self.env,
            "repo.copy_cad_tabelas",
            "REPOSITORIOINGESTION"
        )
        
        # Removing duplicated lines
        df_cad_tabelas.drop_duplicates(inplace=True)
        string_columns = df_cad_tabelas.select_dtypes(include="object")
        df_cad_tabelas[string_columns.columns] = string_columns.apply(lambda x: x.str.lower())
        #df_cad_tabelas = df_cad_tabelas.apply(lambda x: x.str.lower())
        df_cad_tabelas['nome_arq'] = df_cad_tabelas['schema'] + '.' + df_cad_tabelas['tabela']
        df_cad_tabelas = df_cad_tabelas.loc[df_cad_tabelas['nome_arq'] == self.system_table]

        if df_cad_tabelas.empty:
            raise errors.ParamsFile(f'{self.system_table} not found in parameters table (repo.copy_cad_tabelas).')
        if df_cad_tabelas.shape[0] > 1:
            raise errors.ParamsFile(f'{self.system_table} is duplicated in parameters table (repo.copy_cad_tabelas).')

        cad_tabelas_params = {}
        for col in df_cad_tabelas.columns:
            valor = df_cad_tabelas[col].values[0]
            if isinstance(valor, float) and np.isnan(valor):
                valor = None
            if isinstance(valor, str):
                valor = valor.upper()
            cad_tabelas_params[col] = valor

        # Print the system.table params
        msg_log = f"""
\n{35 * '#'}
Loaded repo.copy_cad_tabelas info:
{cad_tabelas_params}
        """
        self.logger.info(msg_log)
        self.logger_io_buffer.write(msg_log)
        return cad_tabelas_params

    def load_s3_cad_colunas_params(self):
        # load
        df_cad_colunas = utils.get_repoingestion_table_as_df(
            self.env,
            "repo.cad_colunas",
            "REPOSITORIOINGESTION"
        )
        # Removing duplicated lines
        df_cad_colunas.drop_duplicates(inplace=True)
        df_cad_colunas = df_cad_colunas.apply(lambda x: x.str.lower())
        df_cad_colunas['nome_arq'] = df_cad_colunas['schema'] + '.' + df_cad_colunas['tabela']
        df_cad_colunas = df_cad_colunas.loc[df_cad_colunas['nome_arq'] == self.system_table]

        if df_cad_colunas.empty:
            raise errors.ParamsFile(f'{self.system_table} not found in parameters table (repo.copy_cad_colunas).')
        if df_cad_colunas.shape[0] > 1:
            raise errors.ParamsFile(f'{self.system_table} is duplicated in parameters table (repo.copy_cad_colunas).')

        cad_colunas_params = {}
        for col in df_cad_colunas.columns:
            valor = df_cad_colunas[col].values[0]
            if isinstance(valor, float) and np.isnan(valor):
                valor = None
            if isinstance(valor, str):
                valor = valor.upper()
            cad_colunas_params[col] = valor

        # Print the system.table params
        msg_log = f"""
\n{35 * '#'}
Loaded repo.cad_colunas info:
{cad_colunas_params}
        """
        self.logger.info(msg_log)
        self.logger_io_buffer.write(msg_log)
        return cad_colunas_params

    def load_s3_jdbc_credentials_params(self, database):
        # load
        s3 = s3fs.S3FileSystem()
        jdbc_credentials = s3.open(f's3://natura-datalake-{self.env}/sqoop/jdbc/{database}.txt', mode="r")
        linhas = jdbc_credentials.readlines()
        jdbc_credentials.close()

        provider = jdbc_url = host = port = bd = username = password = v_driver = ddl_endpoint = None

        for i, linha in enumerate(linhas):
            if '--provider' in linha:
                provider = linhas[i + 1][:-1]
                provider = provider.strip()

            # Get jdbc_url
            if '--connect' in linha:
                jdbc_url = linhas[i + 1][:-1]
                jdbc_url = jdbc_url.strip()
                _connect = jdbc_url.split('//')[1]

                # Get host, port and database
                if ":" in _connect:
                    host = _connect.split(':')[0]
                    port = _connect.split(':')[1].split('/')[0]
                    bd = _connect.split('/')[-1]
                if provider == 'sqlserver':
                    host = _connect.split(';')[0]
                    bd = _connect.split('=')[-1]

            # Get access params
            if '--username' in linha:
                username = linhas[i + 1][:-1]
                username = username.strip()
            if '--password' in linha:
                password = linhas[i + 1][:-1]
                password = password.strip()

            # Get driver (Java class)
            if '--driver' in linha:
                v_driver = linhas[i + 1][:-1]
                v_driver = v_driver.strip()

            # Get ddlEndpoint
            if '--ddlEndpoint' in linha:
                ddl_endpoint = linhas[i + 1][:-1]
                ddl_endpoint = ddl_endpoint.strip()

        params = {
            'host': host,
            'port': port,
            'database': bd,
            'username': username,
            'password': password,
            'v_driver': v_driver,
            'jdbc_url': jdbc_url,
            'provider': provider,
            'ddl_endpoint': ddl_endpoint
        }
        return params


    @abstractmethod
    def run_ingestion(self,
                      system_table: str,
                      partition: str,
                      reproc: bool):
        raise NotImplementedError
