import traceback
from datetime import datetime
import time

from pyspark.sql import SparkSession
import s3fs
import numpy as np

from ingestion_jdbc.ingestion_jdbc import IngestionJDBC
from utils import utils
from utils import errors


class LandingToRawJDBC(IngestionJDBC):
    def __init__(self, env: str, logger, logger_io_buffer, spark: SparkSession):
        IngestionJDBC.__init__(self, env, logger, logger_io_buffer)  # Using constructor Base Classe
        self.spark = spark
        self.layer_destination = 'raw'
        self.layer_source = 'landing'
        self.delimiter = '\001'
        self.path_landing = None
        self.path_raw = None

    def _run_ingestion_without_partition(self, table):
        self.path_landing = f"""s3://{self.bucket_name}/landing/{self.system_table}-0/"""
        self.path_raw = f"""s3://{self.bucket_name}/raw/{table}/"""
        df = self.spark.read.csv(path=self.path_landing, header=False, sep=self.delimiter)
        df.write \
            .mode("OVERWRITE") \
            .format("csv") \
            .option("delimiter", self.delimiter) \
            .option("nullValue", '\\N') \
            .option("emptyValue", '\\N') \
            .option("compression", "gzip") \
            .save(self.path_raw)
        time.sleep(10)
        total_registers = self.spark.read.csv(path=self.path_raw, header=False, sep=self.delimiter).count()
        msg = f"{self.system_table} saved. Rows = {total_registers}"
        self.logger.info(msg)
        self.logger_io_buffer.write(msg)
        s3fs.S3FileSystem().rm(path=self.path_landing, recursive=True)
        return total_registers

    @utils.monitor_time
    def run_ingestion(self,
                      system_table: str,
                      partition: str,
                      reproc: bool = False):
        try:
            # Create variables
            self.system_table = system_table
            self.system_table = self.system_table.lower()
            table = self.system_table.rsplit(".", 1)[1]
            s3 = s3fs.S3FileSystem()

            # Get data from params file and check
            cad_tabelas_params = self.load_s3_cad_tabelas_params()
            cad_arq_params = self.load_s3_cad_arq_params()
            incremtal_overwrite = cad_arq_params['incremtal_overwrite']
            delta_column = cad_tabelas_params['delta_column']
            #where = cad_tabelas_params['where']

            # Specify the load type (Delta or Full)
            # self.load_type is FULL by default. Case where is filled in cad_tabelas.csv then,
            # self.load_type change to DELTA
            self.load_type = "DELTA" if delta_column else self.load_type

            # Specify save mode
            save_mode = 'APPEND'
            if incremtal_overwrite == 'O' or reproc:
                save_mode = 'OVERWRITE'

            # Saving without partition
            if not partition:
                total_registers = self._run_ingestion_without_partition(table)
                return 0, None, total_registers, self.load_type

            # Create partition list for inserting
            partition_list = self._get_partition_to_insert(partition)

            # Insertion!
            # TODO: Vale a pena alterar para inserção de várias partições ao mesmo tempo?
            #  Como fazer o check count nesse caso?
            # Reference for parallel process: # https://stackoverflow.com/questions/45543396/dropping-multiple-partitions-in-impala-hive

            total_registers = 0
            for ref in partition_list:
                self.path_landing = f"""s3://{self.bucket_name}/landing/{self.system_table}-{ref}/"""
                self.path_raw = f"""s3://{self.bucket_name}/raw/{table}/{ref}/"""

                if s3.exists(self.path_landing):
                    if save_mode == 'OVERWRITE':
                        # Removing hive reference
                        self._removing_hive_partition(table, ref)

                        # Removing s3 folder
                        self._removing_s3_partition(table, ref)
                        time.sleep(10)
                    # TODO: O nome das tabelas pode se repetir entre sistemas, na raw e na trusted,
                    #  deveríamos considerar o uso do sistema_tabela
                    #self.path_raw = f"""s3://{self.bucket_name}/raw/{system_table}/{ref}/"""

                    df = self.spark.read.csv(path=self.path_landing, header=False, sep=self.delimiter)

                    # Count source (landing)
                    self.count_source = df.count()
                    """
                    For hive interpreting null use:
                    .option("nullValue", '\\N') \
                    .option("emptyValue", '\\N') \
                    """
                    self.logger.info(f"Writing {ref}")
                    if self.count_source != 0:
                        df.write \
                            .mode(save_mode) \
                            .format("csv") \
                            .option("delimiter", self.delimiter) \
                            .option("nullValue", '\\N') \
                            .option("emptyValue", '\\N') \
                            .option("compression", "gzip") \
                            .save(self.path_raw)
                        time.sleep(2)
                        query = f"""ALTER TABLE raw.{table}
                                    ADD IF NOT EXISTS PARTITION 
                                    (anomesdia={ref}) LOCATION '{self.path_raw}'"""
                        self.spark.sql(query)
                        time.sleep(10)
                        self._check_count_jdbc(system_table=self.system_table, partition=ref)
                        total_registers += self.count_source
                    else:
                        dh = datetime.today().strftime("%d/%m/%Y - %H:%M:%S")
                        msg = f"{dh} - SUCCESSFULLY: But the {self.path_landing} has no data to be inserted"
                        self.logger.info(msg)
                        self.logger_io_buffer.write(msg)
                    # Remove from landing
                    s3.rm(path=self.path_landing, recursive=True)
                else:
                    msg = f"Path {self.path_landing} doesnt exists."
                    self.logger.info(msg)
                    self.logger_io_buffer.write(msg)

            return 0, None, total_registers, self.load_type
        except Exception as msg_error:
            traceback.print_exc()
            self.logger.exception(msg_error)
            self.logger_io_buffer.write(traceback.format_exc())
            return -1, msg_error, 0, self.load_type
