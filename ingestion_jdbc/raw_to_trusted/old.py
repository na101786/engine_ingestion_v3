import traceback

from base_class.ingestion_lake import IngestionLake
from utils import utils
from utils import errors


class RawToTrustedJDBC(IngestionLake):
    def __init__(self, env: str):
        self.env = env
        self.bucket_name = f"natura-datalake-{env}"
        self.spark = RawToTrustedJDBC._create_spark_session()
        self.layer = 'trusted'

    def _check_count_jdbc(self, table, ref):
        count_raw = f"SELECT count(*) AS count FROM raw.{table} WHERE anomesdia={ref}"
        result_count_raw = self.spark.sql(count_raw).first()['count']

        count_trusted = f"SELECT count(*) AS count FROM trusted.{table} WHERE anomesdia={ref}"
        result_count_trusted = self.spark.sql(count_trusted).first()['count']

        if result_count_raw == result_count_trusted:
            print("\n\n###########################")
            print("SUCCESSFULLY: NUMBER OF ROWS IN RAW AND TRUSTED ARE MATCH.")
            print(f"Table raw.{table} saved!")
            print(f"Partition => anomesdia = {ref}")
            print(f"Number of rows saved = {result_count_trusted}")
            print("###########################\n\n")
        else:
            self._removing_hive_partition(table, ref)
            self._removing_s3_partition(table, ref)
            print("\n\n###########################")
            error_msg = "Number of rows in raw and trusted aren't match."
            print("ERROR: ", error_msg)
            print(f"Number of rows in RAW = {result_count_raw}")
            print(f"Number of rows in TRUSTED = {result_count_trusted}")
            print(f"Partition {ref} removed")
            print("###########################\n\n")
            raise errors.CountMatch(error_msg)
        return None

    def transfer_raw_to_trusted_jdbc(self,
                                    system_table: str,
                                    partition: str,
                                    reproc: bool = False):
        try:
            # check system_table param: Format must be system.table. Ex: siscpt.XXXX
            if len(system_table.split('.')) == 1:
                msg_error = "Use system dot table sintaxe. Ex: siscpt.item_pedido"
                raise errors.SintaxeError(msg_error)

            # Create variables
            system_table = system_table.lower()
            df_cad_tabelas = utils.load_s3_cad_tabelas_params(self.env, system_table)
            df_cad_arq = utils.load_s3_cad_arq_params(self.env, system_table)

            table = system_table.rsplit(".", 1)[1]
            database = df_cad_tabelas['servidor'].values[0].upper()
            incremtal_overwrite = df_cad_arq['incremtal_overwrite'].values[0].upper()
            save_mode = 'append'
            if incremtal_overwrite == 'O' or reproc:
                save_mode = 'overwrite'

            del df_cad_tabelas, df_cad_arq  # remove from ram memory

            # Check database source
            if database == 'O01DW':
                msg_error = "Data from O01DW can't go to trusted! Contact coedatagovernance@natura.net"
                raise errors.InvalidLakeAction(msg_error)

            # Create partition list for inserting
            partition_list = [partition]
            if '-' in partition:
                partition_list = utils.create_range_partitions_list(partition=partition)
            elif ',' in partition:
                partition_list = utils.create_specific_partitions_list(partition=partition)

            # Insertion!
            for ref in partition_list:
                if save_mode == 'overwrite':
                    # Removing hive reference
                    self._removing_hive_partition(self.layer, table, ref)

                    # Removing s3 folder
                    self._removing_s3_partition(self.layer, table, ref)

                # TODO: Testar se appenda as informações. hoje nosso processo parace que apenda,
                #  mesmo estando overwrite, por quê?
                query = f"""SELECT * FROM raw.{table} WHERE anomesdia = {ref}"""
                insert = F"""INSERT OVERWRITE TABLE trusted.{table} PARTITION(anomesdia) {query}"""
                self.spark.sql(insert)

            return 0, None

        except Exception as msg_error:
            traceback.print_exc()
            return -1, msg_error
