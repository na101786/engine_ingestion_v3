import pyspark
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
import argparse
import os

from utils import utils
from ingestion_jdbc.raw_to_trusted.raw_to_trusted_jdbc import RawToTrustedJDBC
from ingestion_jdbc.raw_to_bau.raw_to_bau_jdbc import RawToBauJDBC
from ingestion_jdbc.landing_to_raw.landing_to_raw_jdbc import LandingToRawJDBC
from ingestion_jdbc.jdbc_to_landing.source_to_landing_jdbc import JDBCToLanding

'''
Autor: Felipe Rebelo
Data: 23/03/2022
Objetivo: Componente para realizar carga historica
'''

####PASSO A PASSO DO PROGRAMA####
'''
1. Recebe e valida variaveis:
    -tabela
    -periodo
    -campo de data

2. Se conecta na origem e realiza a captura trazendo 1 arquivo com o periodo selecionado
    -colocar validacao de tamanho?
    -colocar confirmacao?

3. Realiza a ingestão do arquivo na camada RAW em uma partição "dummy"
    -estipular uma data e colocar overwrite?
    -partição com timestamp?

4. Com os dados na RAW, realizar o insert na TRUSTED, reparticionando o dado
    -dependendo do escopo pode demorar


'''

"""
    spark-submit \
    /home/hadoop/engine_ingestion_lake_layers/engine_ingestion.py \
    -env dev \
    -system_table siscpt.pedido \
    -table_name_comp dynamics \
    -source_type jdbc \
    -partition XXXXXX \
    -user BRXXXXX \
    --raw_to_trusted \
    --reproc
"""


####funcoes####
def create_spark_session():
    spark = SparkSession.builder \
        .appName("SparkHistoricalProcessing") \
        .enableHiveSupport() \
        .config("hive.metastore.client.factory.class","com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory") \
        .getOrCreate()

    spark.sparkContext.setLogLevel('ERROR')
    return spark



####inicio####


####variaveis####

parser = argparse.ArgumentParser()
parser.add_argument('-env', nargs='?', metavar='Environment', type=str,
                    help='String: Environment to run. Options: [dev, prd]',
                    choices=['dev', 'prd'],
                    required=True,
                    default="prd")
parser.add_argument('-system_table', nargs='?', metavar='System.table', type=str,
                    help='Sting: System and table to ingest. Ex: siscpt.item_pedido',
                    required=True)
parser.add_argument('-source_type', nargs='?', metavar='source_type', type=str,
                    help='Sting: Source type: jdbc, or file, or etc.',
                    required=True,
                    choices=['jdbc'])
parser.add_argument('-period', nargs='?', metavar='period', type=str,
                    help='String: Period to Capture. Ex1: monthly. Ex2: annual. Ex3: semiannual. ',
                    required=True)
parser.add_argument('--jdbc_to_landing', nargs='?', const=True, metavar='jdbc_to_landing', type=bool,
                    help='Boolean: Stage of ingestion to run. Load data from jdbc source to landing.',
                    default=False)
parser.add_argument('--landing_to_raw', nargs='?', const=True, metavar='landing_to_raw', type=bool,
                    help='Boolean: Stage of ingestion to run. Load data from landing to raw.',
                    default=False)
parser.add_argument('--raw_to_trusted', nargs='?', const=True, metavar='raw_to_trusted', type=bool,
                    help='Boolean: Stage of ingestion to run. Load data from raw to trusted.',
                    default=False)
parser.add_argument('--raw_to_bau', nargs='?', const=True, metavar='raw_to_bau', type=bool,
                    help='Boolean: Stage of ingestion to run. Load data from raw to bau.',
                    default=False)
parser.add_argument('-user', nargs='?', metavar='Natura user', type=str,
                    help='Natura user',
                    required=True)


# Capture args
args = parser.parse_args()
env = args.env
system_table = args.system_table
table = system_table.rsplit(".", 1)[1]
source_type = args.source_type
jdbc_to_landing = args.jdbc_to_landing
landing_to_raw = args.landing_to_raw
raw_to_trusted = args.raw_to_trusted
raw_to_bau = args.raw_to_bau
user = args.user
#
period = args.period
delta_field = args.period




print(args,env,system_table,table,source_type,jdbc_to_landing,landing_to_raw,raw_to_trusted,raw_to_bau,sep='\n')



curr_user = os.getlogin()


lake = JDBCToLanding(env=env, logger=logger, logger_io_buffer=logger_io_buffer, spark=spark)
            exec_status, error_message, count_destination, load_type = lake.run_ingestion(system_table=system_table,
                                                                                          partition=partition,
                                                                                          reproc=reproc,
                                                                                          no_tdk=no_tdk)








#######################################################
'''
pandas_df = pd.read_csv(filename, sep='|', header=None)
#print(pandas_df)

###cria sessao spark
spark = create_spark_session()

###transforma df pandas em df spark
sparkDF=spark.createDataFrame(pandas_df)

sparkDF.printSchema()
sparkDF.show()
'''




