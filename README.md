# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Realizar a ingestão de origens JDBC usando spark
* Version: prod

1 - Vai até uma origem relacional via spark JDBC, salva os dados na landing e faz a comparação da quantidade de registros salvos versus origem (count na landing e na origem). Caso o count não esteja correto a ingestão é interrompida com o erro de Count Match.

2 - Transfere os dados da landing para a camada RAW, também fazendo o count entre as duas camadas.

3 - Copia os dados da RAW para a camada TRUSTED ou BAU, também fazendo o count entre as camadas.

4 - Para todas as etapas o log é salvo em dois destinos:
Em um arquivo localizado no caminho **s3://natura-datalake-prd/log_engine_ingestion/log_runnings_details/**
Na tabela **repo.log_resume_ingestion** localizada no banco **repositorioingestion** (coedados-repositorioingestion-prd.c5sphcqmorre.us-east-1.rds.amazonaws.com)

### How do I get set up? ###

* Criar uma pasta chamada engine_ingestion_lake_layers, no caminho /home/hadoop/ e colocar todos os arquivos lá.
* Caso altere o componente, atualizar o arquivo modulos.zip
* Criar um arquivo com as credenciais de acesso ao banco no caminho: s3://natura-datalake-{env}/sqoop/jdbc/
* O arquivo de credenciais deve ter o nome do banco com extensão TXT. Exemplo: O01DW.TXT; CRMBI-PRD.TXT
* O arquivo de credenciais deve seguir o seguinte template:

`#`

`# Arquivo de configuracao de conexao`

`# Configuracao para Oracle O01DW`

`#`

`--provider`

`oracle`

`--connect`

`jdbc:oracle:thin:@//dm01pr-scan.natura.com.br:15030/o01dw`

`--username`

`XXXXXXXX`

`--password`

`YYYYYY`

`--driver`

`oracle.jdbc.driver.OracleDriver`


`# FIM`

`#`

### How to use in EMR ###

* JDBC para landing:

spark-submit --driver-memory 10g --num-executors 10 --executor-cores 2 --executor-memory 3g --conf spark.dynamicAllocation.maxExecutors=8 /home/hadoop/engine_ingestion_lake_layers/engine_ingestion.py -env prd -system_table dbo.contact -source_type jdbc -partition 20210101 -user coedados --jdbc_to_landing

* Landing para raw:

spark-submit --driver-memory 10g --num-executors 10 --executor-cores 2 --executor-memory 3g --conf spark.dynamicAllocation.maxExecutors=8 /home/hadoop/engine_ingestion_lake_layers/engine_ingestion.py -env prd -system_table dbo.contact -source_type jdbc -partition 20210101 -user coedados --landing_to_raw

* raw para trusted/bau

spark-submit --driver-memory 10g --num-executors 10 --executor-cores 2 --executor-memory 3g --conf spark.dynamicAllocation.maxExecutors=8 /home/hadoop/engine_ingestion_lake_layers/engine_ingestion.py -env prd -system_table dbo.contact -source_type jdbc -partition 20210101 -user coedados --raw_to_bau

### Demo video ###
* Assista a um vídeo de demonstração no Link [https://web.microsoftstream.com/video/7a27b908-4d10-4357-8aab-887f6f5e5ef2]

### Who do I talk to? ###

* Guilherme Mochon
* Fernando Cerussi
* Felipe Rebelo