import argparse
import sys
from datetime import datetime
import os
from datetime import timedelta
import traceback

import s3fs

from utils import utils
from ingestion_jdbc.raw_to_trusted.raw_to_trusted_jdbc import RawToTrustedJDBC
from ingestion_jdbc.raw_to_bau.raw_to_bau_jdbc import RawToBauJDBC
from ingestion_jdbc.landing_to_raw.landing_to_raw_jdbc import LandingToRawJDBC
from ingestion_jdbc.jdbc_to_landing.source_to_landing_jdbc_carga import JDBCToLanding
from logs import log_stats
from logs.log_resume_ingestion import LogResumeIngestion


def insert_statistic_log(df, env, table, logger, logger_io_buffer, partition=None, layer=None):
    extract_log = log_stats.LogStats(env=env, table=table)
    msg = f"Initializing statistic log"
    logger.info(msg)
    logger_io_buffer.write(msg)
    df_summary = extract_log.extract_log_stats(df, partition=partition, layer=layer)
    df_summary.printSchema()

    msg = f"Saving statistic log in PostgreSQL repositorioingestion"
    logger.info
    logger_io_buffer.write(msg)
    extract_log.save_stats_in_postgresql(df_summary)


@utils.monitor_time
def main():
    """
    yarn logs -applicationId

    spark-submit --py-files /home/hadoop/engine_ingestion_lake_layers/modulos.zip --master yarn --deploy-mode cluster \

    spark-submit \
    /home/hadoop/engine_ingestion_lake_layers/engine_ingestion.py \
    -env dev \
    -system_table siscpt.pedido \
    -table_name_comp dynamics \
    -source_type jdbc \
    -partition XXXXXX \
    -user BRXXXXX \
    --raw_to_trusted \
    --reproc
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('-env', nargs='?', metavar='Environment', type=str,
                        help='String: Environment to run. Options: [dev, prd]',
                        choices=['dev', 'prd'],
                        required=True,
                        default="prd")
    parser.add_argument('-system_table', nargs='?', metavar='System.table', type=str,
                        help='Sting: System and table to ingest. Ex: siscpt.item_pedido',
                        required=True)
    parser.add_argument('-source_type', nargs='?', metavar='source_type', type=str,
                        help='Sting: Source type: jdbc, or file, or etc.',
                        required=True,
                        choices=['jdbc'])
    parser.add_argument('-partition', nargs='?', metavar='partition', type=str,
                        help='String: Partition to ingestion. Ex1: 20200101. Ex2: 20200101-20200131. Ex3: 20200101,20200201,20200201 ',
                        required=True)
    parser.add_argument('--reproc', nargs='?', const=True, metavar='XXX', type=bool,
                        help='Boolean: reproc drop hive partition and delete s3 folder.',
                        default=False)
    parser.add_argument('--jdbc_to_landing', nargs='?', const=True, metavar='jdbc_to_landing', type=bool,
                        help='Boolean: Stage of ingestion to run. Load data from jdbc source to landing.',
                        default=False)
    parser.add_argument('--landing_to_raw', nargs='?', const=True, metavar='landing_to_raw', type=bool,
                        help='Boolean: Stage of ingestion to run. Load data from landing to raw.',
                        default=False)
    parser.add_argument('--raw_to_trusted', nargs='?', const=True, metavar='raw_to_trusted', type=bool,
                        help='Boolean: Stage of ingestion to run. Load data from raw to trusted.',
                        default=False)
    parser.add_argument('--raw_to_bau', nargs='?', const=True, metavar='raw_to_bau', type=bool,
                        help='Boolean: Stage of ingestion to run. Load data from raw to bau.',
                        default=False)
    parser.add_argument('-user', nargs='?', metavar='Natura user', type=str,
                        help='Natura user',
                        required=True)
    parser.add_argument('--no_tdq', nargs='?', const=True, metavar='raw_to_bau', type=bool,
                        help='Boolean: Stage of ingestion to run. Load data from raw to bau.',
                        default=False)

    # Capture args
    args = parser.parse_args()
    env = args.env
    system_table = args.system_table
    table = system_table.rsplit(".", 1)[1]
    source_type = args.source_type
    partition = args.partition
    reproc = args.reproc
    jdbc_to_landing = args.jdbc_to_landing
    landing_to_raw = args.landing_to_raw
    raw_to_trusted = args.raw_to_trusted
    raw_to_bau = args.raw_to_bau
    user = args.user
    stage = None
    partition = eval(partition) if partition == "None" else partition
    no_tdk = args.no_tdq

    # Create SparkSession
    spark = utils.create_spark_session()

    # Check submition mode
    """
    if env == 'prd':
        for item in spark.sparkContext.getConf().getAll():
            config_deploy_mode = "spark.submit.deployMode" in item[0] and "cluster" in item[1]
            if not config_deploy_mode:
                msg_error = "You need to submit your spark application using deploy mode cluster." \
                            "Ex: spark-submit --py-files /home/hadoop/engine_ingestion_lake_layers/modulos.zip" \
                            " --master yarn --deploy-mode cluster"
                raise utils.errors.InvalidLakeAction(msg_error)
    """

    # Create loggin using logging.StreamHandler() level INFO
    data_run = datetime.today().strftime("%Y%m%d")
    logger = utils.create_logger(env, data_run)

    # Create s3 object from s3fs API. https://s3fs.readthedocs.io/en/latest/api.html
    s3 = s3fs.S3FileSystem()

    # Create loggin using s3 object
    log_file_name = f"{system_table.replace('.', '_')}__{data_run}.log"
    log_path_s3 = f"s3://natura-datalake-{env}/log_engine_ingestion/log_runnings_details"
    logger_io_buffer = s3.open(f'{log_path_s3}/{log_file_name}', 'a')

    # Running...
    #start = (datetime.today() - timedelta(hours=3)).strftime("%Y-%m-%d %H:%M:%S")
    start = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    msg_log = f"\n\nSTART:::::::::{50 * '#'}\nRunning started at: {start} (UTC)"
    logger.info(msg_log)
    logger_io_buffer.write(msg_log)

    if len(system_table.split('.')) == 1:
        error_message = "Use 'system dot table' sintaxe. Ex: siscpt.item_pedido"
        exec_status = -1
    elif source_type == 'jdbc':
        if jdbc_to_landing:
            stage = 'jdbc_to_landing'
            msg_log = f"\nStage = {stage}"
            logger.info(msg_log)
            logger_io_buffer.write(msg_log)
            lake = JDBCToLanding(env=env, logger=logger, logger_io_buffer=logger_io_buffer, spark=spark)
            exec_status, error_message, count_destination, load_type = lake.run_ingestion(system_table=system_table,
                                                                                          partition=partition,
                                                                                          reproc=reproc,
                                                                                          no_tdk=no_tdk)
        elif landing_to_raw:
            stage = 'landing_to_raw'
            msg_log = f"\nStage = {stage}"
            logger.info(msg_log)
            logger_io_buffer.write(msg_log)
            lake = LandingToRawJDBC(env=env, logger=logger, logger_io_buffer=logger_io_buffer, spark=spark)
            exec_status, error_message, count_destination, load_type = lake.run_ingestion(system_table=system_table,
                                                                                          partition=partition,
                                                                                          reproc=reproc)
        elif raw_to_trusted:
            stage = 'raw_to_trusted'
            msg_log = f"\nStage = {stage}"
            logger.info(msg_log)
            logger_io_buffer.write(msg_log)
            lake = RawToTrustedJDBC(env=env, logger=logger, logger_io_buffer=logger_io_buffer, spark=spark)
            exec_status, error_message, count_destination, load_type = lake.run_ingestion(system_table=system_table,
                                                                                          partition=partition,
                                                                                          reproc=reproc)
            if exec_status == 0:
                if '-' not in partition and ',' not in partition:
                    try:
                        if partition:
                            df = spark.read.load(f"s3://natura-datalake-{env}/trusted/{table}/anomesdia={partition}")
                        else:
                            df = spark.read.load(f"s3://natura-datalake-{env}/trusted/{table}/")

                        insert_statistic_log(df, env, table, logger, logger_io_buffer, partition, layer='trusted')
                    except Exception as msg_error:
                        traceback.print_exc()
                        logger_io_buffer.write("Log doesn't created")
                        logger_io_buffer.write(traceback.format_exc())
                        logger.info(traceback.format_exc())

        elif raw_to_bau:
            stage = 'raw_to_bau'
            msg_log = f"\nStage = {stage}"
            logger.info(msg_log)
            logger_io_buffer.write(msg_log)
            lake = RawToBauJDBC(env=env, logger=logger, logger_io_buffer=logger_io_buffer, spark=spark)
            exec_status, error_message, count_destination, load_type = lake.run_ingestion(system_table=system_table,
                                                                                          partition=partition,
                                                                                          reproc=reproc
                                                                                          )
            if exec_status == 0:
                if '-' not in partition and ',' not in partition:
                    try:
                        if partition:
                            df = spark.read.load(f"s3://natura-datalake-{env}/bau/{table}/anomesdia={partition}")
                        else:
                            df = spark.read.load(f"s3://natura-datalake-{env}/bau/{table}/")

                        insert_statistic_log(df, env, table, logger, logger_io_buffer, partition, layer='bau')
                    except Exception as msg_error:
                        traceback.print_exc()
                        logger_io_buffer.write("Log doesn't created")
                        logger_io_buffer.write(traceback.format_exc())
                        logger.info(traceback.format_exc())

    #end = (datetime.today() - timedelta(hours=3)).strftime("%Y-%m-%d %H:%M:%S")
    end = datetime.today().strftime("%Y-%m-%d %H:%M:%S")

    # Check status and error message
    if not error_message:
        error_class = None
    else:
        if isinstance(type(error_message), str):
            error_class = 'Sintaxe error'
        else:
            error_class = str(type(error_message))

    # Saving running details in log file
    msg_log = f"""
RUNNING DETAILS INFO:
start_time (UTC) = {start}
end_time (UTC)= {end}
exec_status = {exec_status}
error_class = {error_class}
error_message = {error_message}
\n{100 * '-'}
"""
    logger.info(msg_log)
    logger_io_buffer.write(msg_log)
    logger_io_buffer.close()

    # Create general log CSV
    if error_message:
        error_message = str(error_message)
        error_message = error_message.replace("'", "")
        error_message = error_message.replace('"', "")
        error_class = str(error_class).replace("'", "")
        error_message = error_message.split(os.linesep)[0]
        error_message = error_message.split("\\n")[0]

    # Hive log foi trocado por log no postgresql repositorioingestion.repo.log_resume_ingestion
    # log_info = [user, 'engine_ingestion.py', stage, system_table, partition, load_type, start, end,
    #             str(exec_status), str(count_destination), str(error_class), error_message]
    # log_info = [f"'{campo}'" for campo in log_info]
    # log_info = ",".join(log_info)
    #
    # query = f"""INSERT INTO TABLE log.log_runnings_history PARTITION (anomesdia={data_run}) VALUES ({log_info})"""
    # print(query)
    # spark.sql(query)
    #start = datetime.strptime(start, "%Y-%m-%d %H:%M:%S")
    #end = datetime.strptime(end, "%Y-%m-%d %H:%M:%S")

    log_postgresql = [user, stage, system_table, str(partition), start, end,
                      exec_status, count_destination, error_class, error_message]

    print('log_postgresql = ', log_postgresql)

    LogResumeIngestion(env=env, table=table).save_resume_in_postgresql(log_postgresql)
    logger.info('log saved!')

    if exec_status != 0:
        sys.exit(-1)


if __name__ == '__main__':
    main()
