#!/bin/bash
#
# This script installs Oracle Instant Client. It must be run 
# in the same directory as the zipped client 
#
# Author: Gabriel R. Pereira <gabrielrichard@natura.net>
# Version: 1.0
# 
# Requirements:
# OS
#    - CentOS (x64)
# Tools
#    - pip
#    - yum
# Files
#    - zipped oracle client (instantclient-basic-linux.x64-XX.X.0.0.0dbru.zip)

export INSTANCE_ID=$(curl --silent http://169.254.169.254/latest/meta-data/instance-id)
export REGION=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone  | sed -e "s/.$//")
export S3_BUCKET_UTILS=$(aws ec2 describe-tags --region $REGION --filter "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=s3_bucket_utils" --output=text | cut -f5)

FILE=instantclient-basic-linux.x64-19.3.0.0.0dbru.zip
CLIENT_DIR=instantclient_19_3
aws s3 cp $S3_BUCKET_UTILS/packages/$FILE .
# Exits if file does not exist in directory
if [ ! -f "./$FILE" ]; then
    echo "$FILE not found in directory."
    exit 1
fi
sudo mkdir -p /opt/oracle
sudo cp ./$FILE /opt/oracle/
cd /opt/oracle
sudo unzip $FILE
sudo yum install -y libaio
sudo sh -c "echo /opt/oracle/$CLIENT_DIR > /etc/ld.so.conf.d/oracle-instantclient.conf"
sudo ldconfig
sudo sh -c "echo export LD_LIBRARY_PATH=/opt/oracle/$CLIENT_DIR:$LD_LIBRARY_PATH > /etc/profile.d/oracle_env.sh"
pip install cx_Oracle


