import argparse
import sys
from datetime import datetime
import os

import s3fs

from ingestion_jdbc.jdbc_to_landing.source_to_landing_historic_jdbc2 import JDBCToLandingHistoric2
from utils import utils


@utils.monitor_time
def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('-env', nargs='?', metavar='Environment', type=str,
                        help='String: Environment to run. Options: [dev, prd]',
                        choices=['dev', 'prd'],
                        required=True,
                        default="prd")
    parser.add_argument('-system_table', nargs='?', metavar='System.table', type=str,
                        help='Sting: System and table to ingest. Ex: siscpt.item_pedido',
                        required=True)
    parser.add_argument('-source_type', nargs='?', metavar='source_type', type=str,
                        help='Sting: Source type: jdbc, or file, or etc.',
                        required=True,
                        choices=['jdbc'])
    parser.add_argument('-partition', nargs='?', metavar='partition', type=str,
                        help='String: Partition to ingestion. Ex1: 20200101. Ex2: 20200101-20200131. Ex3: 20200101,20200201,20200201 ',
                        required=True)
    args = parser.parse_args()
    env = args.env
    system_table = args.system_table
    source_type = args.source_type
    partition = args.partition

    # Create SparkSession
    spark = utils.create_spark_session()

    # Create loggin using logging.StreamHandler() level INFO
    data_run = datetime.today().strftime("%Y%m%d")
    logger = utils.create_logger(env, data_run)

    # Create s3 object from s3fs API. https://s3fs.readthedocs.io/en/latest/api.html
    s3 = s3fs.S3FileSystem()

    # Create loggin using s3 object
    log_file_name = f"{system_table.replace('.', '_')}_historic__{data_run}.log"
    log_path_s3 = f"s3://natura-datalake-{env}/log_engine_ingestion/log_runnings_details"
    logger_io_buffer = s3.open(f'{log_path_s3}/{log_file_name}', 'a')

    # Running...
    # start = (datetime.today() - timedelta(hours=3)).strftime("%Y-%m-%d %H:%M:%S")
    start = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
    msg_log = f"\n\nSTART:::::::::{50 * '#'}\nRunning started at: {start} (UTC)"
    logger.info(msg_log)
    logger_io_buffer.write(msg_log)

    if len(system_table.split('.')) == 1:
        error_message = "Use 'system dot table' sintaxe. Ex: siscpt.item_pedido"
        exec_status = -1

    stage = 'jdbc_to_landing_HISTORIC'
    msg_log = f"\nStage = {stage}"
    logger.info(msg_log)
    logger_io_buffer.write(msg_log)
    lake = JDBCToLandingHistoric2(env=env, logger=logger, logger_io_buffer=logger_io_buffer, spark=spark)
    exec_status = lake.run_ingestion(system_table=system_table,
                                     partition=partition,
                                     reproc=False)
    if exec_status == 0:
        msg = "HISTORIC INGESTION OK"
        logger.info(msg)
        logger_io_buffer.write(msg)
        logger_io_buffer.close()
    else:
        msg = "ERROR IN HISTORIC INGESTION"
        logger.info(msg)
        logger_io_buffer.write(msg)
        logger_io_buffer.close()
        sys.exit(-1)


if __name__ == '__main__':
    main()
