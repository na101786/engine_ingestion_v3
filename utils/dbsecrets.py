import boto3


def get_autentication(secret_id):
    import boto3
    client = boto3.client('secretsmanager', region_name='us-east-1')
    return client.get_secret_value(SecretId=secret_id)
    
#user= get_autentication("coedados/rds/repositorioingestion/username")
#password = get_autentication("coedados/rds/repositorioingestion/password")
#print(user['SecretString'])
#print(password['SecretString'])
