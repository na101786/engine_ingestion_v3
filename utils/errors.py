

class ParamsFile(Exception):
    def __init__(self, msg):
        self.msg = msg


class SintaxeError(Exception):
    def __init__(self, msg):
        self.msg = msg


class InvalidLakeAction(Exception):
    def __init__(self, msg):
        self.msg = msg


class CountMatch(Exception):
    def __init__(self, msg):
        self.msg = msg
