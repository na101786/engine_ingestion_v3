import s3fs


def load_s3_jdbc_credentials_params(env, database):
    # load
    s3 = s3fs.S3FileSystem()
    database = database.upper()
    jdbc_credentials = s3.open(f's3://natura-datalake-{env}/sqoop/jdbc/{database}.txt', mode="r")
    linhas = jdbc_credentials.readlines()
    jdbc_credentials.close()

    provider = jdbc_url = host = port = bd = username = password = v_driver = ddl_endpoint = None

    for i, linha in enumerate(linhas):
        if '--provider' in linha:
            provider = linhas[i + 1][:-1]
            provider = provider.strip()

        # Get jdbc_url
        if '--connect' in linha:
            jdbc_url = linhas[i + 1][:-1]
            jdbc_url = jdbc_url.strip()
            _connect = jdbc_url.split('//')[1]

            # Get host, port and database
            if ":" in _connect:
                host = _connect.split(':')[0]
                port = _connect.split(':')[1].split('/')[0]
                bd = _connect.split('/')[-1]
            if provider == 'sqlserver':
                host = _connect.split(';')[0]
                bd = _connect.split('=')[-1]

        # Get access params
        if '--username' in linha:
            username = linhas[i + 1][:-1]
            username = username.strip()
        if '--password' in linha:
            password = linhas[i + 1][:-1]
            password = password.strip()

        # Get driver (Java class)
        if '--driver' in linha:
            v_driver = linhas[i + 1][:-1]
            v_driver = v_driver.strip()

        # Get ddlEndpoint
        if '--ddlEndpoint' in linha:
            ddl_endpoint = linhas[i + 1][:-1]
            ddl_endpoint = ddl_endpoint.strip()

    params = {
        'host': host,
        'port': port,
        'database': bd,
        'username': username,
        'password': password,
        'v_driver': v_driver,
        'jdbc_url': jdbc_url,
        'provider': provider,
        'ddl_endpoint': ddl_endpoint
    }
    return params