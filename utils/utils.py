import sys
import time
import logging
from datetime import datetime as dt
from datetime import timedelta
from functools import wraps
from utils.dbsecrets import get_autentication
from utils.get_credentials import load_s3_jdbc_credentials_params
from utils import errors
from pyspark.sql import SparkSession
#from aws_logging_handlers.S3 import S3Handler, S3Stream

def create_spark_session():
    spark = SparkSession.builder \
        .config("hive.exec.dynamic.partition", "true") \
        .config("hive.exec.dynamic.partition.mode", "nonstrict") \
        .config("spark.sql.catalogImplementation", "hive") \
        .config("spark.sql.parquet.writeLegacyFormat", "true") \
        .config("hive.metastore.client.factory.class",
                "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory") \
        .getOrCreate()

    spark.sparkContext.setLogLevel('ERROR')
    return spark

def get_repoingestion_table_as_df(
        env: str,
        dbtable: str,
        dbname: str
        ):
    """
    Devolve um DF das tabelas
    do repositorio ingestion 
    """
    spark = SparkSession.builder.getOrCreate()
    spark.sparkContext.setLogLevel('ERROR')

    paramns = load_s3_jdbc_credentials_params(env, dbname)
    jdbc_url = paramns['jdbc_url']
    user = get_autentication("coedados/rds/repositorioingestion/username")['SecretString']
    password = get_autentication("coedados/rds/repositorioingestion/password")['SecretString']
    driver = paramns['v_driver']

    df = spark.read.format("jdbc") \
        .option("url", jdbc_url) \
        .option("driver", driver) \
        .option("dbtable", dbtable) \
        .option("user", user) \
        .option("password", password).load()

    if df.count() == 0:
        print("\n\n\n############################")
        print("ERROR: Database, or table, or schema not found in query:\n")
        print(dbtable)
        print("The program will be end.")
        sys.exit(-1)
        print("############################\n\n\n")

    df_pandas = df.toPandas()
    print(df_pandas)
    return df_pandas 


def monitor_time(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        t1 = time.time()
        result = func(*args, **kwargs)
        t2 = time.time()
        logger.info(f"Time elapsed for '{func.__name__}' was {(t2 - t1) / 60:.2f} minutes - {round(t2 - t1, 2)} seg.")
        return result
    return wrapper


def create_range_partitions_list(partition: str):
    partitions = sorted(partition.split("-"), key=int, reverse=True)
    partition_newest = dt.strptime(partitions[0], '%Y%m%d')
    partition_oldest = partition_newest
    partition_oldest = dt.strptime(partitions[1], '%Y%m%d') if len(partitions) > 1 else partition_oldest

    numdays = abs((partition_newest - partition_oldest).days)
    # if numdays > 31:
    #     msg_error = "You can't process a partition range greater than 1 month (31 days)." \
    #                 "You need to adjust your tag -partition"
    #     raise errors.InvalidLakeAction(msg_error)
    partition_list = [(partition_oldest + timedelta(days=dia)).strftime("%Y%m%d")
                       for dia in range(numdays + 1)]
    return partition_list


def create_specific_partitions_list(partition: str):
    partition_list = sorted(partition.split(","), key=int, reverse=False)
    return partition_list


def create_logger(env: str, data_run: str):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter("%(levelname)-4s - %(message)s")

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    # doesn't work in submit mode cluster
    #file_handler = logging.FileHandler(f'/home/hadoop/engine_ingestion_lake_layers/logging_teste_{data_run}.log', mode='w')
    #file_handler.setFormatter(formatter)
    #logger.addHandler(file_handler)

    # doesn't work
    #s3_handler = S3Handler(bucket=f"natura-datalake-{env}", key="/landing/aaaaa_test_log.log")
    #s3_handler.setFormatter(formatter)
    #logger.addHandler(s3_handler)

    return logger
