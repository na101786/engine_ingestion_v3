import boto3
import pandas as pd
import numpy as np
from abc import ABC, abstractmethod
from datetime import datetime
from utils import errors
from utils import utils


class IngestionLake(ABC):
    def __init__(self, env: str, logger, logger_io_buffer):
        self.env = env
        self.bucket_name = f"natura-datalake-{env}"
        self.layer_destination = None
        self.layer_source = None
        self.logger = logger
        self.logger_io_buffer = logger_io_buffer
        self.count_source = None
        self.count_destination = 0

    def _removing_hive_partition(self, table, partition):
        # Remove a referencia do HIVE
        self.logger.info(f"cleaning hive partition - {partition}")
        alter = f"""ALTER TABLE {self.layer_destination}.{table} DROP IF EXISTS PARTITION (anomesdia={partition})"""
        self.spark.sql(alter)
        return None

    def _removing_s3_partition(self, table, partition):
        # Removing s3 folder
        self.logger.info(f"cleaning s3 folder partition - {partition}")
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(self.bucket_name)
        file_delete = f"{self.layer_destination}/{table}/anomesdia={partition}/"
        bucket.objects.filter(Prefix=file_delete).delete()
        return None

    def _get_partition_to_insert(self, partition):
        partition_list = [partition]  # One partition
        if '-' in partition:
            partition_list = utils.create_range_partitions_list(partition=partition)  # Range
        elif ',' in partition:
            partition_list = utils.create_specific_partitions_list(partition=partition)  # Specifics
        return partition_list

    # implementar aqui o load_s3_cad_arq
    def load_s3_cad_arq_params(self):
        """
        Load cad_arq.csv in s3://natura-datalake-{env}/scripts/cad_arq.csv

        :param system_table System and table to load. Sintaxe system.table
        :return: pandas DF with one line.
        """
        # load
        df_cad_arq = utils.get_repoingestion_table_as_df(
            self.env,
            "repo.copy_cad_arq",
            "REPOSITORIOINGESTION"
        )
        
        # Removing duplicated lines
        df_cad_arq.drop_duplicates(inplace=True)
        # Filter the system.table register
        df_param = df_cad_arq.loc[df_cad_arq['nome_arq'] == self.system_table]
        # Check if exists and duplicated system.table.
        if df_param.empty:
            raise errors.ParamsFile(f'{self.system_table} not found in parameters file.')
        if df_param.shape[0] > 1:
            raise errors.ParamsFile(f'{self.system_table} is duplicated in parameters file.')

        cad_arq_params = {}
        for col in df_param.columns:
            valor = df_param[col].values[0]
            if isinstance(valor, float) and np.isnan(valor):
                valor = None
            if isinstance(valor, str):
                valor = valor.upper()
            cad_arq_params[col] = valor

        # Print the system.table params
        msg_log = f"""
Loaded repo.cad_arq info:
{cad_arq_params}
        """
        self.logger.info(msg_log)
        self.logger_io_buffer.write(msg_log)
        return cad_arq_params

    def _check_count_jdbc(self, system_table, partition):
        """
        Works with landing-to-raw, raw-to-trusted
        :param system_table:
        :param partition:
        :return:
        """
        # Landing doesn't have hive table
        table = system_table.rsplit(".", 1)[1]

        count_destination = f"SELECT count(*) AS COUNT FROM {self.layer_destination}.{table} WHERE anomesdia={partition}"
        self.count_destination = self.spark.sql(count_destination).first()['COUNT']
        dh = datetime.today().strftime("%d/%m/%Y - %H:%M:%S")

        if self.count_source == self.count_destination:
            msg_log = f"""
SUCCESSFULLY: NUMBER OF ROWS IN {self.layer_source} AND {self.layer_destination} ARE MATCH.
Table {self.layer_destination}.{table} saved!
Partition => anomesdia = {partition}
Number of rows saved = {self.count_destination}
###########################
            """
            self.logger.info(msg_log)
            self.logger_io_buffer.write(msg_log)
            #return self.count_destination
        else:
            self._removing_hive_partition(table, partition)
            self._removing_s3_partition(table, partition)
            error_msg = f"Number of rows in {self.layer_source} and {self.layer_destination} aren't match."
            msg_log = f"""
\n###########################
{dh} - ERROR: {error_msg}
Number of rows in {self.layer_source} = {self.count_source}
Number of rows in {self.layer_destination} = {self.count_destination}
Partition {partition} removed
###########################
            """
            self.logger.info(msg_log)
            self.logger_io_buffer.write(msg_log)
            raise errors.CountMatch(error_msg)

    @abstractmethod
    def run_ingestion(self,
                      system_table: str,
                      partition: str,
                      reproc: bool):
        raise NotImplementedError

