from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import *
import pandas as pd

from utils import get_credentials


class LogResumeIngestion():
    def __init__(self,
                 env: str,
                 table: str):
        self.env = env
        self.spark = SparkSession.builder.getOrCreate()
        self.table = table
        self.log_postgresql = None

    def _create_df_spark(self):
        self.log_postgresql = pd.DataFrame(self.log_postgresql).T

        my_schema = StructType([
            StructField("username", StringType()),
            StructField("stage", StringType()),
            StructField("system_table", StringType()),
            StructField("partition_loaded", StringType()),
            StructField("start_timestamp", StringType()),
            StructField("end_timestamp", StringType()),
            StructField("exec_status", IntegerType()),
            StructField("count_destination", IntegerType()),
            StructField("error_class", StringType()),
            StructField("error_message", StringType())
        ])

        df_log_postgre = self.spark.createDataFrame(data=self.log_postgresql, schema=my_schema)
        df_log_postgre = df_log_postgre.select(
            'username',
            'stage',
            'system_table',
            'partition_loaded',
            df_log_postgre['start_timestamp'].cast('timestamp'),
            df_log_postgre['end_timestamp'].cast('timestamp'),
            'exec_status',
            'count_destination',
            'error_class',
            'error_message')

        df_log_postgre = df_log_postgre.withColumn("timestamp_log", F.current_timestamp())

        return df_log_postgre

    def save_resume_in_postgresql(self,
                                  log_postgresql: list,
                                  table="repo.log_resume_ingestion",
                                  database="repositorioingestion"):

        self.log_postgresql = log_postgresql

        params = get_credentials.load_s3_jdbc_credentials_params(self.env, database=database)

        username = params['username']
        password = params['password']
        jdbc_url = params['jdbc_url']
        driver = params['v_driver']

        df = self._create_df_spark()

        df.write \
            .format("jdbc") \
            .option("url", jdbc_url) \
            .option("dbtable", table) \
            .option("user", username) \
            .option("password", password) \
            .option("driver", driver) \
            .option("batchsize", 10000) \
            .mode("append") \
            .save()
