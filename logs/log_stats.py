from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import *
import pandas as pd
pd.options.display.float_format = '{:.2f}'.format

from utils import get_credentials


class LogStats():
    def __init__(self,
                 env: str,
                 table: str):
        self.env = env
        self.spark = SparkSession.builder.getOrCreate()
        self.table = table

    @staticmethod
    def transpose_df(df, columns, pivotCol):
        columns_value = list(map(lambda x: str("'") + str(x) + str("',") + str(x), columns))
        stack_cols = ','.join(x for x in columns_value)

        df_1 = df\
            .selectExpr(pivotCol,
                        "stack(" + str(len(columns)) + "," + stack_cols + ")") \
            .select(pivotCol, "col0", "col1")

        final_df = df_1\
            .groupBy(F.col("col0"))\
            .pivot(pivotCol)\
            .agg(F.concat_ws("", F.collect_list(F.col("col1")))) \
            .withColumnRenamed("col0", pivotCol)

        return final_df

    def _create_null_1_count(self, df):
        df_null_1 = df.select([F.count(F.when(df[c].isNull(), c)).alias(c) for c in df.columns])
        df_null_1 = df_null_1\
            .toPandas().T\
            .reset_index()\
            .rename(columns={'index': 'summary', 0: 'count_null_t1'})

        my_schema = StructType([
            StructField("summary", StringType(), True),
            StructField("count_null_t1", IntegerType(), True)
        ])

        df_null_1 = self.spark.createDataFrame(df_null_1, schema=my_schema)
        return df_null_1

    def _create_count_non_numerics(self, df, list_non_numeric_types):
        df_count = df.select([F.count(c).alias(c) for c in list_non_numeric_types])
        df_count = df_count\
            .toPandas().T\
            .reset_index()\
            .rename(columns={'index': 'summary', 0: 'count'})

        my_schema = StructType([
            StructField("summary", StringType(), True),
            StructField("count", IntegerType(), True)
        ])

        df_count = self.spark.createDataFrame(df_count, schema=my_schema)
        return df_count

    def _create_n_uniques_non_numerics(self, df, list_non_numeric_types):
        df_n_unique = df.agg(*(F.countDistinct(df[c]).alias(c) for c in list_non_numeric_types))
        df_n_unique = df_n_unique\
            .toPandas().T\
            .reset_index()\
            .rename(columns={'index': 'summary', 0: 'n_unique'})

        my_schema = StructType([
            StructField("summary", StringType(), True),
            StructField("n_unique", IntegerType(), True)
        ])

        df_n_unique = self.spark.createDataFrame(df_n_unique, schema=my_schema)
        return df_n_unique

    def _create_top_freq_non_numerics(self, df, list_non_numeric_types):
        def extract_freq(df, col):
            df_p = df.groupBy(col) \
                .agg(F.count(col).alias('_count')) \
                .agg(F.first(col).alias('top'), F.max('_count').alias('freq')) \
                .withColumn('summary', F.lit(col))
            return df_p

        df_top_freq = extract_freq(df, list_non_numeric_types[0])

        for c in list_non_numeric_types[1:]:
            df_top_freq = df_top_freq.union(extract_freq(df, c))

        return df_top_freq

    def _extract_stats_numeric_cols(self, df):
        """
        This function create a spark DataFrame with the numerical statics.
        Each column of a original df will be a row in the final spark DataFrame.
        In the final spark DataFrame, the columns will be the statics metrics
        """
        list_numeric_types = [col[0] for col in df.dtypes if col[1] != 'string']

        df_numeric_summary = df.select(list_numeric_types).summary()
        df_numeric_summary = LogStats.transpose_df(df=df_numeric_summary,
                                                   columns=list_numeric_types,
                                                   pivotCol='summary')

        df_numeric_summary = df_numeric_summary \
            .withColumn('column_type', F.lit('numeric')) \
            .withColumn('n_unique', F.lit(None)) \
            .withColumn('top', F.lit(None)) \
            .withColumn('freq', F.lit(None))

        df_numeric_summary = df_numeric_summary\
            .select('summary',
                    'column_type',
                    'count',
                    'min',
                    'max',
                    'mean',
                    'stddev',
                    '25%',
                    '50%',
                    '75%',
                    'n_unique',
                    'top',
                    'freq')

        return df_numeric_summary

    def _extract_stats_non_numeric_cols(self, df):
        list_non_numeric_types = [col[0] for col in df.dtypes if col[1] == 'string']
        df_count = self._create_count_non_numerics(df, list_non_numeric_types)
        df_n_unique = self._create_n_uniques_non_numerics(df, list_non_numeric_types)
        df_top_freq = self._create_top_freq_non_numerics(df, list_non_numeric_types)

        df_non_numeric_summary = df_count \
            .join(df_n_unique, on='summary', how='left') \
            .join(df_top_freq, on='summary', how='left')

        df_non_numeric_summary = df_non_numeric_summary \
            .withColumn('column_type', F.lit('non_numeric')) \
            .withColumn('min', F.lit(None)) \
            .withColumn('max', F.lit(None)) \
            .withColumn('mean', F.lit(None)) \
            .withColumn('stddev', F.lit(None)) \
            .withColumn('25%', F.lit(None)) \
            .withColumn('50%', F.lit(None)) \
            .withColumn('75%', F.lit(None))

        df_non_numeric_summary = df_non_numeric_summary\
            .select('summary',
                    'column_type',
                    'count',
                    'min',
                    'max',
                    'mean',
                    'stddev',
                    '25%',
                    '50%',
                    '75%',
                    'n_unique',
                    'top',
                    'freq')
        return df_non_numeric_summary

    def extract_log_stats(self, df, partition, layer):
        df_numeric_summary = self._extract_stats_numeric_cols(df)
        #df_non_numeric_summary = self._extract_stats_non_numeric_cols(df)

        #df_summary = df_numeric_summary.union(df_non_numeric_summary)
        df_summary = df_numeric_summary
        df_null_1 = self._create_null_1_count(df)
        df_summary = df_summary.join(df_null_1, on='summary', how='left')

        df_summary = df_summary\
            .withColumnRenamed('summary', 'table_fields')\
            .withColumnRenamed('count', 'column_count')\
            .withColumnRenamed('min', 'column_min')\
            .withColumnRenamed('max', 'column_max')\
            .withColumnRenamed('mean', 'column_mean')\
            .withColumnRenamed('stddev', 'column_std')\
            .withColumnRenamed('25%', 'q_25')\
            .withColumnRenamed('50%', 'q_50')\
            .withColumnRenamed('75%', 'q_75')

        df_summary = df_summary \
            .withColumn("table_layer", F.lit(layer)) \
            .withColumn("table_name", F.lit(self.table)) \
            .withColumn("partition_loaded", F.lit(str(partition))) \
            .withColumn("timestamp_log", F.current_timestamp())

        df_summary = df_summary\
            .select('table_layer',
                    'table_name',
                    'table_fields',
                    'column_type',
                    'partition_loaded',
                    df_summary['column_count'].cast('integer'),
                    df_summary['count_null_t1'].cast('integer'),
                    df_summary['column_min'].cast('double'),
                    df_summary['column_max'].cast('double'),
                    df_summary['column_mean'].cast('double'),
                    df_summary['column_std'].cast('double'),
                    df_summary['q_25'].cast('integer'),
                    df_summary['q_50'].cast('integer'),
                    df_summary['q_75'].cast('integer'),
                    df_summary['n_unique'].cast('integer'),
                    df_summary['top'].cast('string'),
                    df_summary['freq'].cast('long'),
                    'timestamp_log')

        return df_summary

    def save_stats_in_postgresql(self,
                                  df,
                                  table="repo.log_ingestion_statistic",
                                  database="repositorioingestion"):

        params = get_credentials.load_s3_jdbc_credentials_params(self.env, database=database)

        username = params['username']
        password = params['password']
        jdbc_url = params['jdbc_url']
        driver = params['v_driver']

        df.write \
            .format("jdbc") \
            .option("url", jdbc_url) \
            .option("dbtable", table) \
            .option("user", username) \
            .option("password", password) \
            .option("driver", driver) \
            .option("batchsize", 10000) \
            .mode("append") \
            .save()

#
# def main():
#     """
# spark-submit \
# /home/hadoop/engine_ingestion_lake_layers/logs/log_stats.py
#     """
#     spark = SparkSession.builder \
#         .config("hive.exec.dynamic.partition", "true") \
#         .config("hive.exec.dynamic.partition.mode", "nonstrict") \
#         .config("spark.sql.catalogImplementation", "hive") \
#         .config("spark.sql.parquet.writeLegacyFormat", "true") \
#         .config("hive.metastore.client.factory.class",
#                 "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory") \
#         .getOrCreate()
#
#     spark.sparkContext.setLogLevel('ERROR')
#
#     df = spark.read.load("s3a://natura-datalake-dev/trusted/pedido/anomesdia=20201102")
#
#     print("Creating...")
#     extract_log = LogStats(env='dev', table='pedido')
#     df_summary = extract_log.extract_log_stats(df, partition='20201102', layer='trusted')
#     print(df_summary.first())
#
#     print("Saving...")
#     extract_log.save_stats_in_postgresql(df_summary)
#
#
# if __name__ == '__main__':
#     main()
